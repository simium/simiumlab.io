import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './app.css';
import 'jquery/dist/jquery.min.js';
import 'bootstrap/dist/js/bootstrap.bundle.js';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    NavLink
} from "react-router-dom";
import AllClubs from "./components/allClubs/AllClubs";
import Matches from "./components/matches/Matches";
import Match from "./components/matches/Match";
import MyClub from "./components/myClub/MyClub";
import ClubDetail from "./components/clubDetail/ClubDetail";
import Home from "./components/home/Home";
import GameProvider from "./components/gameProvider/GameProvider";
import FirstContract from "./contract/FirstContract.json";
import FootballPlayer from "./contract/FootballPlayer.json";
import { Drizzle } from '@drizzle/store'
import AcademyPlayerDetail from "./components/players/academyPlayerDetail/AcademyPlayerDetail";
import ClubPlayerDetail from "./components/players/clubPlayerDetail/ClubPlayerDetail";
import PlayerDetail from "./components/players/playerDetail/PlayerDetail";

const drizzleOptions = {
    contracts: [FirstContract, FootballPlayer],
    events: {
        FirstContract: ["ClubOffered", "Transfer", "ClubBidPlaced", "ClubBought", "ClubNoLongerForSale", "ClubBidWithdrawn"],
        FootballPlayer: ["PlayerTransfer", "PlayerMinted", "PlayerPutUpForSale", "PlayerBidFor", "PlayerOfferWithdrawn", "PlayerBought", "PlayerBidWithdrawn"]
    }
};

const drizzle = new Drizzle(drizzleOptions);

class App extends React.Component {
    constructor(props) {
        super(props);
        // this.state = { backgroundImage: "../../images/home-backgound.jpg" };
    }

    render() {

        return (
            <GameProvider drizzle={drizzle}>
                {context => {
                    const { drizzle, drizzleState, initialized, gameState } = context;

                    if (!initialized) {
                        return "Loading...";
                    }

                    console.log(gameState);

                    return (
                        <div >
                            <nav className="navbar navbar-expand-md navbar-white bg-dark sticky-top">
                                <div className="container-fluid">
                                    <Link className="navbar-brand logo" to="/">Footium</Link>
                                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                                            name="button" data-target="#navbarResponsive">
                                    </button>
                                    <div className="navbar-selection collapse navbar-collapse" id="navbarResponsive">
                                        <ul className="navbar-nav ml-auto">
                                            <li className="nav-item ">
                                                <NavLink activeClassName="activeButton" className="nav-link" to="/clubs">All Clubs</NavLink>
                                            </li>
                                            <li className="nav-item">
                                                <NavLink activeClassName="activeButton" className="nav-link" to="/matches">Matches</NavLink>
                                            </li>
                                            <li className="nav-item">
                                                <NavLink activeClassName="activeButton" className="nav-link" to="/myClub">My Club</NavLink>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </nav>

                            <Switch>
                                <Route path="/" exact component={Home}/>
                                <Route path="/clubs" exact component={() => <AllClubs drizzle={drizzle} gameState={gameState}/>}/>
                                <Route path="/clubs/:id" exact component={ (props) => <ClubDetail drizzle={drizzle} id={props.match.params.id} drizzleState={drizzleState} gameState={gameState}/>}/>
                                <Route path="/matches" component={() => <Matches drizzle={drizzle} drizzleState={drizzleState} gameState={gameState} />}/>
                                <Route path="/match/:homeId/:awayId/:timestamp" component={({ match }) => <Match drizzle={drizzle} drizzleState={drizzleState} gameState={gameState} homeClubId={match.params.homeId} awayClubId={match.params.awayId} timestamp={match.params.timestamp} />}/>
                                <Route path="/myClub" component={() => <MyClub drizzle={drizzle} drizzleState={drizzleState} gameState={gameState}/>}/>
                                <Route path="/clubs/:clubId/clubPlayer/:playerId" component={ClubPlayerDetail}/>
                                <Route path="/clubs/:clubId/academyPlayer/:birthYear/:generationId" component={(props) => <AcademyPlayerDetail drizzle={drizzle} clubOwner={drizzleState.accounts[0]} gameState={gameState} params={props.match.params}/>} />
                                <Route path="/players/:playerId" component={({ match }) => <PlayerDetail drizzle={drizzle} drizzleState={drizzleState} gameState={gameState} id={match.params.playerId}/>} />
                            </Switch>
                        </div>
                    );
                }}
            </GameProvider>
        );
    }


}

export default App


