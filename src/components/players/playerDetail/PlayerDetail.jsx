import React from "react";
import { Player } from "footium-engine";
import PlayerStatsCard from "../playerStatsCard/PlayerStatsCard";
import ClubDropdown from "../../helpers/ClubDropdown";
import '../../clubDetail/clubInfo/clubInfo.css';
class PlayerOwnerView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            offer: 0
        }
    }

    getOfferView() {
        const { drizzle, player } = this.props;

        const { fromWei } = drizzle.web3.utils;

        if (!player.isForSale) {
            return <h4>You have not offered this player for sale</h4>
        } else {
            return <h4>You are offering this player for: {fromWei(player.price)} ETH</h4>
        }
    }

    onChangeHandler = (event) => {
        if (event.target.value.length === 0) {
            this.setState({
                offer: 0
            });
        } else {
            this.setState({
                offer: event.target.value
            });
        }
    }

    getHighestBid() {
        const { drizzle, player } = this.props;

        const { fromWei } = drizzle.web3.utils;

        if (player.highestBid <= 0) {
            return <h4>No one has placed a bid</h4>
        } else {
            return <h4>The highest bid on the player is: {fromWei(player.highestBid)} ETH</h4>
        }
    }

    submitOfferHandler = (event) => {
        event.preventDefault();

        const { drizzle, player } = this.props;
        const { offer } = this.state;

        const { toWei } = drizzle.web3.utils;

        if (offer <= 0) {
            alert("Invalid number")
        } else {
            drizzle.contracts.FootballPlayer.methods.offerPlayerForSale(player.id, toWei(offer)).send();
        }
    }

    acceptBidHandler = () => {
        const { drizzle, player } = this.props;

        if (player.highestBid <= 0) {
            alert("No one has placed a bid.")
        } else {
            drizzle.contracts.FootballPlayer.methods.acceptBid(player.id)
                .send()
                .catch((error, receipt) => {
                    console.log(error);
                    console.log(receipt);
                });
        }
    }

    render() {
        return (
            <div className="transaction-container-owner mt-4 p-4">
                <div className="transaction-description pb-2">
                    {this.getOfferView()}
                    {this.getHighestBid()}
                </div>
                <div className="d-flex row justify-content-around ">
                    <form onSubmit={this.submitOfferHandler} className="form-inline my-2 my-lg-0">
                        <input type="text" className="form-control mr-sm-2 ml-2" placeholder="Put an offer"
                               aria-label="offer" onChange={this.onChangeHandler} />
                            <button type="submit" className="btn btn-primary">Offer</button>
                    </form>
                    <button className="btn btn-primary" onClick={this.acceptBidHandler}>Accept bid</button>
                </div>
            </div>
        )
    }
}

class PlayerBuyerView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            bid: 0,
            clubId: null
        };
    }

    putBidHandler = (event) => {
        event.preventDefault();

        const { drizzle, player } = this.props;
        const { bid, clubId } = this.state;

        const { toWei, fromWei } = drizzle.web3.utils;

        if (clubId === null || clubId === 0) {
            alert("You must select a club to perform the operation.");
        } else if (bid <= 0) {
            alert("You cannot submit a bid of 0.");
        } else if (bid <= fromWei(player.highestBid.toString())) {
            alert("You must submit a higher bid than the current highest bid.");
        } else {
            drizzle.contracts.FootballPlayer.methods.placeBid(player.id, clubId)
                .send({
                    value: toWei(bid)
                })
                .catch((error, receipt) => {
                    console.log(error);
                    console.log(receipt);
                });
        }
    }

    onChangeHandler = (event) => {
        const { value } = event.target;

        if (value.length === 0) {
            this.setState({ bid: 0 });
        } else {
            this.setState({ bid: value });
        }
    }

    getBuyView() {
        const { drizzle, player } = this.props;

        const { fromWei } = drizzle.web3.utils;

        if (!player.isForSale) {
            return <h4>This player is currently not for sale</h4>
        } else {
            return <h4>This player is currently offered for {fromWei(player.price)} ETH</h4>
        }
    }

    buyPlayerHandler = () => {
        const { drizzle, player } = this.props;
        const { clubId } = this.state;

        if (clubId === null || clubId === 0) {
            alert("You must select a club to perform the operation.");
        } else if (!player.isForSale) {
            alert("The owner has not put a price, this player is not for sale.")
        } else {
            drizzle.contracts.FootballPlayer.methods.buyPlayer(player.id, clubId).send({
                value: player.price
            });
        }
    }

    getMyClubIds() {
        const { drizzleState, gameState } = this.props;

        const currentAccount = drizzleState.accounts[0];
        
        const accountData = gameState.owners[currentAccount];

        if (accountData === undefined) {
            return [];
        }

        return accountData.clubIdsOwned;
    }

    handleClubChange = (clubId) => {
        this.setState(state => {
            state.clubId = clubId;

            return state;
        });
    }

    render() {
        const { drizzle, gameState, player } = this.props;

        const { fromWei } = drizzle.web3.utils;
        
        const highestBid = fromWei(player.highestBid.toString());

        return (
            <div className="transaction-container-owner mt-4 p-4">
                <div className="transaction-description pb-2">
                    {this.getBuyView(player.id)}
                    <h4>The highest bid is: {highestBid} ETH</h4>
                </div>
                <div className="d-flex row justify-content-around ">
                    <form className="form-inline my-2 my-lg-0" onSubmit={this.putBidHandler}>
                        <input type="text" className="form-control mr-sm-2 ml-2" placeholder="Put a bid"
                               aria-label="offer" onChange={this.onChangeHandler}/>
                            <button type="submit" className="btn btn-primary">Bid</button>
                    </form>
                    <button className="btn btn-primary" onClick={this.buyPlayerHandler}>Buy Player</button>
                </div>
                <ClubDropdown gameState={gameState} clubIds={this.getMyClubIds()} onChange={this.handleClubChange} />
            </div>
        )
    }
}

const PlayerBuyingView = ({ drizzle, drizzleState, gameState, player, club }) => {
    if (club.owner === drizzleState.accounts[0]) {
        return (
            <PlayerOwnerView
                player={player} gameState={gameState}
                drizzle={drizzle} drizzleState={drizzleState} />
        );
    }

    return (
        <PlayerBuyerView
            player={player} gameState={gameState}
            drizzle={drizzle} drizzleState={drizzleState} />
    );
};

const PlayerDetail = ({ drizzle, drizzleState, gameState, id }) => {
    const player = gameState.players[id];

    const club = gameState.clubs[player.clubId];
    const playerStats = Player.current(player)(new Date());

    return (
        <div className="container-fluid">
            <div className="club-player-page">
                <PlayerStatsCard gameState={gameState} player={player} playerStats={playerStats} />
                <div className="container col-lg-10">
                    <PlayerBuyingView drizzle={drizzle} drizzleState={drizzleState} gameState={gameState} player={player} club={club} />
                </div>
            </div>
        </div>
    );
};

export default PlayerDetail;
