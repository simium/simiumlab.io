import React from "react";
import '../../clubDetail/clubInfo/clubInfo.css'
import ClubPlayerInfo from "./ClubPlayerInfo";
import ClubPlayerBuyerView from "./ClubPlayerBuyerView";
import ClubPlayerOwnerView from "./ClubPlayerOwnerView";
import ClubInfo from "../../clubDetail/clubInfo/ClubInfo";
import ClubPlayers from "../../clubDetail/clubPlayers/ClubPlayers";
import ClubAcademy from "../../clubDetail/clubAcademy/ClubAcademy";

class ClubPlayerDetail extends React.Component {

    render() {

        return (
            <div className="container-fluid ">
                    <ClubPlayerInfo/>
                    <ClubPlayerBuyerView/>
                    <ClubPlayerOwnerView/>
            </div>
        )

    }
}

export default ClubPlayerDetail