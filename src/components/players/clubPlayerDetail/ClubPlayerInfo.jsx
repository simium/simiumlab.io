import React from "react";
import '../../clubDetail/clubInfo/clubInfo.css'

class ClubPlayerInfo extends React.Component {

    render() {

        return (
            <div className="club-info d-flex justify-content-center align-items-center mt-4 pt-2 pb-1">
                <img className="club-img mr-5" src={require('./test.png')} width="200" height="200" alt="the img"/>
                <div className="player-attribute">
                    <h1 className="mt-5">Player Name</h1>
                    <ul>
                        <li>Owned by: Nobody</li>
                        <li>Club: xxxx</li>
                        <li>Birthday: 1999.01.01</li>
                        <li>Talent: 500</li>
                        <li>Rating: 500</li>
                        <li>Leadership: 500</li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default ClubPlayerInfo