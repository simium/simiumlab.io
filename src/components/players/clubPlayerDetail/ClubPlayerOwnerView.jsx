import React from "react";
import '../../clubDetail/clubInfo/clubInfo.css'

class ClubPlayerOwnerView extends React.Component {

    render() {

        return (
            <div className="club-info  mt-4 p-4">
                <div className="offer-info pb-2">
                    <h4>You are offering this player for: xxx ETH</h4>
                    <h4>The highest bid on the player is: xxx ETH</h4>
                </div>
                <div className="d-flex row justify-content-around ">

                    <form className="form-inline my-2 my-lg-0">
                        <input type="text" className="form-control mr-sm-2 ml-2" placeholder="Put an offer"
                               aria-label="offer"/>
                            <a className="btn btn-primary" href="#">Offer</a>
                    </form>
                    <a className="btn btn-primary" href="#">Accept Bid</a>

                </div>
            </div>
        )
    }
}

export default ClubPlayerOwnerView