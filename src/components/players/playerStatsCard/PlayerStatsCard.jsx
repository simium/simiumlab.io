import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBrain, faFistRaised, faFutbol, faBirthdayCake, faArrowUp, faCalendar, faChartLine} from "@fortawesome/free-solid-svg-icons";
import '../../clubDetail/clubInfo/clubInfo.css'
const PlayerStatsCard = ({ gameState, player, playerStats }) => {
    const club = gameState.clubs[player.clubId];

    const stadiumStyle = {
        backgroundImage: `url(https://jordanlord.co.uk/footium/stadium-${club.stadiumPictureId}.png)`
    };

    return (
        <div>
            <div className="club-graphics-container" style={stadiumStyle}>
                <div className="club-logo-container">
                    <div className="club-logo">
                        <img className="logo-img" src={`https://jordanlord.co.uk/footium/face-${player.facePictureId}.png`} width="100%" alt="face"/>
                        <div className="star-rating">
                            <center><h1 className="mt-2">{player.firstName + " " + player.lastName}</h1></center>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container col-lg-10">
                <div className="club-details">
                    <div className="detail-item">
                        <div className="detail-icon-container">
                            <FontAwesomeIcon icon={faFutbol}/>
                        </div>
                        <div className="detail-value-container ">
                            <div className="detail-description">
                                Club
                            </div>
                            <div className="detail-value">
                                {gameState.clubs[player.clubId].name}
                            </div>
                        </div>
                    </div>
                    <div className="detail-item">
                        <div className="detail-icon-container">
                            <FontAwesomeIcon icon={faBirthdayCake}/>
                        </div>
                        <div className="detail-value-container ">
                            <div className="detail-description">
                                Birth Date
                            </div>
                            <div className="detail-value">
                                {player.birthDate.toDateString()}
                            </div>
                        </div>
                    </div>
                    <div className="detail-item">
                        <div className="detail-icon-container">
                            <FontAwesomeIcon icon={faCalendar}/>
                        </div>
                        <div className="detail-value-container ">
                            <div className="detail-description">
                                Age
                            </div>
                            <div className="detail-value">
                                {playerStats.age}
                            </div>
                        </div>
                    </div>
                    <div className="detail-item">
                        <div className="detail-icon-container">
                            <FontAwesomeIcon icon={faArrowUp}/>
                        </div>
                        <div className="detail-value-container ">
                            <div className="detail-description">
                                Experience
                            </div>
                            <div className="detail-value">
                                {playerStats.experience.toFixed(1)}
                            </div>
                        </div>
                    </div>
                    <div className="detail-item">
                        <div className="detail-icon-container">
                            <FontAwesomeIcon icon={faBrain}/>
                        </div>
                        <div className="detail-value-container ">
                            <div className="detail-description">
                                Talent
                            </div>
                            <div className="detail-value">
                                {playerStats.talent.toFixed(1)}
                            </div>
                        </div>
                    </div>
                    <div className="detail-item">
                        <div className="detail-icon-container">
                            <FontAwesomeIcon icon={faChartLine}/>
                        </div>
                        <div className="detail-value-container ">
                            <div className="detail-description">
                                Rating
                            </div>
                            <div className="detail-value">
                                {playerStats.rating.toFixed(1)}
                            </div>
                        </div>
                    </div>
                    <div className="detail-item">
                        <div className="detail-icon-container">
                            <FontAwesomeIcon icon={faFistRaised}/>
                        </div>
                        <div className="detail-value-container ">
                            <div className="detail-description">
                                Leadership
                            </div>
                            <div className="detail-value">
                                {playerStats.leadership.toFixed(1)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>


        // <div className="club-info d-flex justify-content-center align-items-center mt-4 pt-2 pb-1">
        //     <img className="club-img mr-5"  src={require('./test.png')} width="200" height="200" alt="the img"/>
        //     <div className="player-attribute">
        //         <h1 className="mt-5">{player.firstName + " " + player.lastName}</h1>
        //         <ul>
        //             <li>Club: {gameState.clubs[player.clubId].name}</li>
        //             <li>Birthday: {player.birthDate.toDateString()}</li>
        //             <li>Age: {playerStats.age} years old</li>
        //             <li>Experience: {playerStats.experience.toFixed(1)}</li>
        //             <li>Talent: {playerStats.talent.toFixed(1)}</li>
        //             <li>Rating: {playerStats.rating.toFixed(1)}</li>
        //             <li>Leadership: {playerStats.leadership.toFixed(1)}</li>
        //         </ul>
        //     </div>
        // </div>
    );
};

export default PlayerStatsCard;
