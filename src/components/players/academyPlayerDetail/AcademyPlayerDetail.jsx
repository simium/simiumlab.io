import React from "react";
import '../../clubDetail/clubInfo/clubInfo.css'
import { Player, Game } from "footium-engine";
import PlayerStatsCard from "../playerStatsCard/PlayerStatsCard";

class AcademyPlayerDetail extends React.Component {
    constructor(props) {
        super(props);
        this.signPlayer = this.signPlayer.bind(this);
    }

    getAcademyPlayer() {
        const { gameState, params } = this.props;

        const player = Game.getAcademyPlayer(params)(gameState);

        return player;
    }

    signPlayer() {
        const { drizzle } = this.props;
        const player = this.getAcademyPlayer();

        drizzle.contracts.FootballPlayer.methods.mintPlayer(player.clubId, player.birthDate.getFullYear(),
            player.generationId, player.clubId).send();
    }

    getSignPlayerButton(player, clubs) {
        if (player.id || clubs[player.clubId].owner === null ||clubs[player.clubId].owner !== this.props.clubOwner) {
            return (<div></div>);
        }

        return (
            <div className="club-info d-flex justify-content-center mt-4 p-4">
                <button className="btn btn-secondary" onClick={this.signPlayer}>Sign this player</button>
            </div>
        );
    }

    render() {
        const { gameState, params, id } = this.props;

        params.birthYear = parseInt(params.birthYear);

        const player = Game.getAcademyPlayer(params)(gameState);

        const playerStats = Player.current(player)(new Date());

        return (
            <div className="container-fluid academy-page">
                    <PlayerStatsCard gameState={gameState} player={player} playerStats={playerStats} />

                    {this.getSignPlayerButton(player, gameState.clubs)}
            </div>
        )
    }
}

export default AcademyPlayerDetail;
