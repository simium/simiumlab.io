import React from "react";
import { Owner } from "footium-engine";
import ClubCard from "../allClubs/clubCards/ClubCard";
import './myClub.css';

class MyClub extends React.Component {
    constructor(props) {
        super(props);
        this.clubWithdrawHandler = this.clubWithdrawHandler.bind(this);
        this.playerWithdrawHandler = this.playerWithdrawHandler.bind(this);
    }

    clubWithdrawHandler(){
        const { drizzle } = this.props;
        drizzle.contracts.FirstContract.methods.withdraw().send();
    }

    playerWithdrawHandler() {
        const { drizzle } = this.props;
        drizzle.contracts.FootballPlayer.methods.withdraw().send();
    }

    getPlayerPendingWithdrawals() {
        const { gameState, drizzleState, drizzle } = this.props;

        const { fromWei } = drizzle.web3.utils;

        const currentAccount = drizzleState.accounts[0];

        const accountData = gameState.owners[currentAccount];

        if (accountData === undefined) {
            return 0;
        } else {
            return fromWei(accountData.pendingPlayerWithdrawals.toString());
        }
    }

    getClubPendingWithdrawals() {
        const { gameState, drizzleState, drizzle } = this.props;

        const { fromWei } = drizzle.web3.utils;

        const currentAccount = drizzleState.accounts[0];

        const accountData = gameState.owners[currentAccount];

        if (accountData === undefined) {
            const obj = Owner.create(currentAccount);

            return obj.pendingWithdrawals;
        } else {
            return fromWei(accountData.pendingWithdrawals.toString());
        }
    }

    renderClubCards(){
        const { gameState, drizzleState, drizzle } = this.props;
        const currentAccount = drizzleState.accounts[0];
        const accountData = gameState.owners[currentAccount];

        if (accountData === undefined) {
            return <p style={{color: '#406E8E', fontSize: '1.4rem'}}>You currently don't own any clubs</p>;
        } else {
            const clubsOwned = accountData.clubIdsOwned;

            return (
                <div className="container">
                    <div className="row">
                        {clubsOwned.map((clubId) => <ClubCard drizzle={drizzle} club={gameState.clubs[clubId]} key={clubId}/>)}
                    </div>
                </div>
            )
        }
    }

    render() {
        const { gameState, drizzleState, drizzle } = this.props;
        const currentAccount = drizzleState.accounts[0];
        const accountData = gameState.owners[currentAccount];

        return (
            <div className="container-fluid my-club-page pt-3">
                <div className="col-lg-8 col-md-10 mx-auto">
                    <div className="all-clubs-header-container col-lg-6 col-md-8 mx-auto bg-black  mb-3">
                        <h1 className="all-clubs-header">My Clubs</h1>
                    </div>

                    <div className="withdraw-container d-flex row justify-content-center mb-5 p-4">
                        <div>
                            <h4 className="mt-1 withdraw-description d-inline">Your club pending withdrawals is: {this.getClubPendingWithdrawals()} ETH</h4>
                            <button className="btn btn-primary ml-5 mb-4" onClick={this.clubWithdrawHandler}>Withdraw</button>
                        </div>
                        <div>
                            <h4 className="mt-1 withdraw-description d-inline">Your player pending withdrawals is: {this.getPlayerPendingWithdrawals()} ETH</h4>
                            <button className="btn btn-primary ml-5" onClick={this.playerWithdrawHandler}>Withdraw</button>
                        </div>
                    </div>

                    {this.renderClubCards()}

                </div>
            </div>
        )
    }
}

export default MyClub