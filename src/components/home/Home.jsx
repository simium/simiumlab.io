import React from "react";
import './home.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMeteor } from '@fortawesome/free-solid-svg-icons'
import ClubPlayerCard from "../clubDetail/clubPlayers/ClubPlayerCard";
import {Link} from "react-router-dom";

class Home extends React.Component {
    render() {
        return (
            <div className="home-background">
                <div className="banners row">
                    <h1 className="welcome-banner">Welcome to Footium</h1>
                    <h2 className="own-club-banner">An Ethereum-based football simulation game</h2>
                    <div className="button-container">
                        <Link
                            to={`/clubs`}>
                            <button className="primary-button"><FontAwesomeIcon icon={faMeteor} /> Get Started</button>
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home