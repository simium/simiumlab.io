import React from 'react';
import './displayClubs.css';

class DisplayClubs extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            ownerOf : null
        }
        this.takeOwnership = this.takeOwnership.bind(this);
    }

    componentDidMount() {
        this.updateOwnerOf();
    }

    updateOwnerOf() {
        const { drizzle } = this.props;
        const { clubId } = this.props;

        const ownerOf = drizzle.contracts.FirstContract.methods.ownerOf(clubId).call().then((result) => {
            console.log(clubId);
            this.setState({ ownerOf: result });
        }).catch(error => {
            this.setState({ ownerOf: 'Nobody' });
        });
    }

    takeOwnership(){
        const { clubId } = this.props;
        const drizzle = this.props.drizzle;

        drizzle.contracts.FirstContract.methods.takeOwnership(clubId).send().then(() => {
            this.updateOwnerOf();
        });
    }

    render() {
        const clubId = this.props.clubId;
        const { ownerOf } = this.state;
        console.log(clubId);
        return (
            <div className={"card-container"}>
                <div className={"id-container"}>
                    <p className={"club-id"}>Club {clubId}</p>
                </div>
                <div className={"owner-container"}>
                    <p className={"owner"}>The owner is {ownerOf}</p>
                </div>
                <div className={"button-container"}>
                    <button className={"button"} onClick={this.takeOwnership}>Take Ownership</button>
                </div>
            </div>
        )
    }
}

export default DisplayClubs