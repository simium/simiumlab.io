import React from "react";

const ClubLogo = ({ club, className }) => {
    return (
        <img
            className={className}
            src={`https://jordanlord.co.uk/footium/club-${club.clubBadgeId}.png`}
            alt="club-logo"/>
    );
};

export default ClubLogo;
