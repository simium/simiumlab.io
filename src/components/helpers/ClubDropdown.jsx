import React from "react";

class ClubDropdown extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentClubId: null
        };
    }

    handleChange(clubId) {
        const { onChange } = this.props;

        this.setState(state => {
            state.currentClubId = clubId;

            return state;
        });

        onChange(clubId);
    }

    getCurrentClubName() {
        const { gameState } = this.props;
        const { currentClubId } = this.state;

        if (currentClubId) {
            return gameState.clubs[currentClubId].name;
        }

        return "Select Club";
    }

    render() {
        const { gameState, clubIds } = this.props;
        const { currentClubId } = this.state;

        return (
            <div className="dropdown d-flex justify-content-center mt-4">
                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {this.getCurrentClubName()}
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                  {
                    clubIds.map(clubId => (
                        <button
                            key={clubId}
                            className="dropdown-item"
                            type="button"
                            onClick={e => this.handleChange(clubId)}>
                            
                            {gameState.clubs[clubId].name}
                        </button>)
                    )
                  }
                </div>
            </div>
        );
    }
}

export default ClubDropdown;
