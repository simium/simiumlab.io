import React from "react";

const StadiumBackground = ({ className, children, club }) => {
    const stadiumStyle = {
        backgroundImage: `url(https://jordanlord.co.uk/footium/stadium-${club.stadiumPictureId}.png)`
    };

    return (
        <div className={className} style={stadiumStyle}>
            {children}
        </div>
    );
};

export default StadiumBackground;
