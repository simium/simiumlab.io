import React from "react";
import "./match.css";
import "../clubDetail/clubInfo/clubInfo.css"
import ClubDropdown from "../helpers/ClubDropdown";
import ClubLogo from "../helpers/ClubLogo";
import StadiumBackground from "../helpers/StadiumBackground";

import { MatchSimulator, MatchCommentary } from "footium-engine";

const MatchTime = ({ event }) => {
    if (event.data.matchTime) {
        return (
            <div className="text-tertiary">
                {Math.floor(event.data.matchTime) + "'"}
            </div>
        );
    }

    return "";
};

const CommentaryCard = ({ commentary, event }) => {
    return (
        <div className="bg-secondary p-2 mb-4 rounded">
            <MatchTime event={event} />
            <div className="text-primary my-2">{commentary.text}</div>
        </div>
    );
};

const Squad = ({ squad }) => {
    return (
        <div className="bg-secondary text-primary p-4 rounded">
            <div className="mb-4 text-tertiary">Line-up</div>
            <div>
                {squad.firstTeam.map(player => (
                    <div>
                        {player.firstName + " " + player.lastName}
                        <hr className="my-1" />
                    </div>
                ))}
            </div>

            <div className="mt-4">
                <div className="mb-4 text-tertiary">Substitutes</div>
                <div>
                    {squad.substitutes.map(player => (
                        <div>
                            {player.firstName + " " + player.lastName}
                            <hr className="my-1" />
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
};

class Timer extends React.Component {
    constructor(props) {
        super(props);

        const date = new Date();

        console.log(props.startTime);

        const startTime = parseInt(props.startTime);

        this.state = {
            startTime: startTime,
            timestamp: date.getTime(),
            endTime: startTime + 7200000 // 2 hours
        };
    }

    updateTimestamp(newTimestamp) {
        this.setState(state => {
            state.timestamp = newTimestamp;

            return state;
        });
    }

    handleChange = (event) => {
        console.log(event);

        this.updateTimestamp(event.target.value);
    }

    render() {
        const { children } = this.props;
        const { startTime, timestamp, endTime } = this.state;

        return (
            <div>
                <input
                    className="w-100"
                    type="range" onChange={this.handleChange}
                    value={timestamp} min={startTime} max={endTime} />
                {children(timestamp)}
            </div>
        );
    }
}

const _Match = ({ gameState, homeClubId, awayClubId, timestamp, currentTime }) => {
    const seed = `${homeClubId}-${awayClubId}-${timestamp}`;

    const fixture = {
        homeClubId,
        awayClubId,
        date: new Date(parseInt(timestamp)),
        seed
    };

    const currentDate = new Date(parseInt(currentTime));

    console.log(currentDate);

    const { state, match, events } =
        MatchSimulator.simulateFixture(fixture, gameState)(currentDate);

    const commentary =
        MatchCommentary.fromEvents(match)(events)(seed + "-commentary");

    return (
        <div className="navbar-font">
            <StadiumBackground className="club-graphics-container" club={match.homeClub}>
                <div className="score-container background-height">
                    <h2 className="text-primary p-2">{match.homeClub.name}</h2>
                    <ClubLogo className="mt-2 mx-5" club={match.homeClub} />
                    <h2 className="bg-secondary text-primary p-2 m-1">{state.score[0]}</h2>
                    <h2 className="bg-secondary text-primary p-2 m-1">{state.score[1]}</h2>
                    <ClubLogo className="mt-2 mx-5" club={match.awayClub} />
                    <h2 className="text-primary p-2">{match.awayClub.name}</h2>
                </div>
            </StadiumBackground>
            <div className="container">
                <div className="row">
                    <div className="col-lg-4 pt-4">
                        <Squad squad={match.homeSquad} />
                    </div>
                    <div className="col-lg-4 pt-4">
                        {commentary.map((e, i) => <CommentaryCard commentary={e} event={events[i]} />).reverse()}
                    </div>
                    <div className="col-lg-4 pt-4">
                        <Squad squad={match.awaySquad} />
                    </div>
                </div>
            </div>
        </div>
    );
};

const Match = ({ gameState, homeClubId, awayClubId, timestamp }) => {
    return (
        <Timer startTime={timestamp}>
            {currentTime => <_Match
                currentTime={currentTime}
                gameState={gameState} homeClubId={homeClubId}
                awayClubId={awayClubId} timestamp={timestamp} />}
        </Timer>
    );
};

export default Match;
