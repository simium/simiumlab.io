import React from "react";
import "./match.css";
import ClubDropdown from "../helpers/ClubDropdown";
import ClubLogo from "../helpers/ClubLogo";
import { Link } from "react-router-dom";

import { MatchSimulator } from "footium-engine";

const MatchCard = ({ fixture, gameState, currentDate }) => {
    const { state, match, events } =
        MatchSimulator.simulateFixture(fixture, gameState)(currentDate);

    const homeId = match.homeClubId;
    const awayId = match.awayClubId;
    const timestamp = fixture.date.getTime();

    return (
        <Link to={`/match/${homeId}/${awayId}/${timestamp}`}>
            <div className="d-flex justify-content-between my-2 p-2 navbar-font bg-primary">
                <ClubLogo className="" club={match.homeClub} />
                <div className="d-flex w-100 pr-2 justify-content-end align-items-center">
                    <div className="text-primary">{match.homeClub.name}</div>
                </div>
                <div className="d-flex justify-content-end align-items-center p-2 col-lg-1">
                    <div className="text-center bg-secondary w-100 p-2">{state.score[0]} - {state.score[1]}</div>
                </div>
                <div className="d-flex w-100 pl-2 align-items-center">
                    <div className="text-primary">{match.awayClub.name}</div>
                </div>
                <ClubLogo className="" club={match.awayClub} />
            </div>
        </Link>
    );
};

class Matches extends React.Component {
    render() {
        const { gameState } = this.props;

        const currentDate = new Date(2020, 7, 23, 15, 0, 0, 0);
        const date = new Date(2020, 7, 23, 21, 40, 0, 0);

        const fixtures = [
            { homeClubId: 1, awayClubId: 2, date },
            { homeClubId: 3, awayClubId: 4, date },
            { homeClubId: 5, awayClubId: 6, date },
            { homeClubId: 7, awayClubId: 8, date },
            { homeClubId: 9, awayClubId: 10, date },
            { homeClubId: 11, awayClubId: 12, date },
            { homeClubId: 13, awayClubId: 14, date },
            { homeClubId: 15, awayClubId: 16, date },
            { homeClubId: 17, awayClubId: 18, date },
            { homeClubId: 19, awayClubId: 20, date }
        ].map(fixture => ({
            ...fixture,
            seed: `${fixture.homeClubId}-${fixture.awayClubId}-${fixture.date.getTime()}`
        }));

        return (
            <div className="container col-lg-6">
                {fixtures.map(fixture => (
                    <MatchCard
                        key={fixture.seed} fixture={fixture}
                        gameState={gameState} currentDate={currentDate} />
                ))}
            </div>
        )
    }
}

export default Matches
