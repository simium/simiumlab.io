import { DrizzleContext } from "@drizzle/react-plugin";
import React from 'react';
import * as Engine from "footium-engine";

class GameLoader extends React.Component {
    constructor(props) {
        super(props);

        this.state = null;
    }

    componentDidMount() {
        Engine.engine(this.props.drizzle).subscribe(gameState => {
            this.setState(gameState);
        });
    }

    render() {
        if (this.state === null) {
            return "Loading game state...";
        };

        return this.props.children(this.state);
    }
}

const GameProvider = ({ drizzle, children }) => (
    <DrizzleContext.Provider drizzle={drizzle}>
        <DrizzleContext.Consumer>
            {drizzleContext => {
                if (!drizzleContext.initialized) {
                    return (<p>Loading...</p>);
                }

                return (
                    <GameLoader drizzle={drizzle}>
                        {gameState => {
                            const { drizzleState, initialized } = drizzleContext;

                            return children({
                                drizzle,
                                drizzleState,
                                initialized,
                                gameState
                            });
                        }}
                    </GameLoader>
                );
            }}
        </DrizzleContext.Consumer>
    </DrizzleContext.Provider>
);

export default GameProvider;
