import React from "react";
import './clubInfo.css'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faUser} from '@fortawesome/free-solid-svg-icons'

class ClubInfoExtra extends React.Component {

    render() {
        const { id } = this.props;
        const clubInfo = this.props.gameState.clubs[id];
        console.log(clubInfo);
        return (
            <div className="club-name-container d-flex justify-content-between">
                <div className="club-name-in-detail">
                    {clubInfo.name}
                </div>
                <div className="owner d-flex justify-content-center">
                    <div className="owner-avatar  d-flex align-content-center flex-wrap">
                        <h1><FontAwesomeIcon icon={faUser}/></h1>
                    </div>
                    <div className="owner-name d-flex align-content-center flex-wrap">
                        <div className="owner-text">{ clubInfo.owner == null ? 'Not Owned' : clubInfo.owner }</div>
                        <p className="owner-small-text">(Owner)</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default ClubInfoExtra