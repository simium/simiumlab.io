import React from "react";
import './clubInfo.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faIdCard, faUsers, faTrophy, faFrownOpen, faGripLines } from '@fortawesome/free-solid-svg-icons'

class ClubInfoStats extends React.Component {

    render() {
        const { id } = this.props;
        const clubInfo = this.props.gameState.clubs[id];

        return (
            <div className="club-details">
                <div className="detail-item">
                    <div className="detail-icon-container">
                        <FontAwesomeIcon icon={faIdCard}/>
                    </div>
                    <div className="detail-value-container ">
                        <div className="detail-description">
                            Club ID
                        </div>
                        <div className="detail-value">
                            # {id}
                        </div>
                    </div>
                </div>
                <div className="detail-item">
                    <div className="detail-icon-container">
                        <FontAwesomeIcon icon={faUsers}/>
                    </div>
                    <div className="detail-value-container ">
                        <div className="detail-description">
                            Total Player
                        </div>
                        <div className="detail-value">
                            # {clubInfo.playerIds.length}
                        </div>
                    </div>
                </div>
                <div className="detail-item">
                    <div className="detail-icon-container">
                        <FontAwesomeIcon icon={faTrophy}/>
                    </div>
                    <div className="detail-value-container ">
                        <div className="detail-description">
                            Games Won
                        </div>
                        <div className="detail-value">
                            # No Data
                        </div>
                    </div>
                </div>
                <div className="detail-item">
                    <div className="detail-icon-container">
                        <FontAwesomeIcon icon={faFrownOpen}/>
                    </div>
                    <div className="detail-value-container ">
                        <div className="detail-description">
                            Games Lost
                        </div>
                        <div className="detail-value">
                            # No Data
                        </div>
                    </div>
                </div>
                <div className="detail-item">
                    <div className="detail-icon-container">
                        <FontAwesomeIcon icon={faGripLines}/>
                    </div>
                    <div className="detail-value-container ">
                        <div className="detail-description">
                            Draws
                        </div>
                        <div className="detail-value">
                            # No Data
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ClubInfoStats