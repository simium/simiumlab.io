import React from "react";
import './clubInfo.css'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ClubLogo from "../../helpers/ClubLogo";
import StadiumBackground from "../../helpers/StadiumBackground";

class ClubInfo extends React.Component {

    render() {
        const { id } = this.props;
        const clubInfo = this.props.gameState.clubs[id];

        return (
            <StadiumBackground className="club-graphics-container" club={clubInfo}>
                <div className="club-logo-container">
                    <div className="club-logo">
                        <ClubLogo className="mt-2 w-100" club={clubInfo} />
                        <div className="star-rating">
                            <center><h1><FontAwesomeIcon icon={faStar} /><FontAwesomeIcon icon={faStar} /><FontAwesomeIcon icon={faStar} /></h1></center>
                        </div>
                    </div>
                </div>
            </StadiumBackground>
        )
    }
}

export default ClubInfo
