import React from "react";
import '../clubInfo/clubInfo.css';

class ClubPlayerCard extends React.Component {

    render() {
        const { lastName, facePictureId } = this.props;

        return (
            <div className="player-item">
                <img src={`https://jordanlord.co.uk/footium/face-${facePictureId}.png`} width="100%"/>
                <h5 className="player-name-in-card">{lastName}</h5>
            </div>
        )
    }

}

export default ClubPlayerCard
