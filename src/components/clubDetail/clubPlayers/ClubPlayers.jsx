import React from "react";
import { Link } from "react-router-dom";
import '../clubInfo/clubInfo.css'
import ClubPlayerCard from "./ClubPlayerCard";

class ClubPlayers extends React.Component {

    render() {
        const { players } = this.props;

        if (players.length === 0) {
            return (
                <div className="player-container">
                    <div className="player-container-title">
                        <h3>Players:</h3>
                    </div>
                    <div className="no-player">
                        <h4>No player has been signed for this club.</h4>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="player-container">
                    <div className="player-container-title">
                        <h3>Players:</h3>
                    </div>
                    <div className="player-list">
                        {
                            players.map(player => (
                                <Link
                                    key={player.id}
                                    to={`/players/${player.id}`}>
                                    <ClubPlayerCard lastName={player.lastName} facePictureId={player.facePictureId} />
                                </Link>
                            ))
                        }
                    </div>
                </div>
            );

        }
    }
}

export default ClubPlayers
