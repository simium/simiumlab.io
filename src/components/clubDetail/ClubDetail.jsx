import React from "react";
import './clubDetail.css'
import ClubInfo from "./clubInfo/ClubInfo";
import ClubBuyerView from "./clubBuyerView/ClubBuyerView";
import ClubOwnerView from "./clubOwnerView/ClubOwnerView";
import ClubNotOwnedView from "./clubNotOwnedView/ClubNotOwnedView";
import ClubPlayers from "./clubPlayers/ClubPlayers";
import ClubAcademy from "./clubAcademy/ClubAcademy";
import ClubInfoExtra from "./clubInfo/ClubInfoExtra";
import ClubInfoStats from "./clubInfo/ClubInfoStats";

class ClubDetail extends React.Component {
    constructor(props) {
        super(props);
        const { gameState } = this.props;
        this.state = {
            id: this.props,
            owner: gameState.clubs[this.props.id].owner
        }
    }

    render() {
        const { id } = this.props;
        const { gameState, drizzleState, drizzle } = this.props;
        let renderContent;

        if (this.state.owner === null) {
            renderContent = <ClubNotOwnedView updateOwner={this.updateOwner} drizzle={drizzle} id={id} />;
        } else if (this.state.owner === drizzleState.accounts[0]) {
            renderContent = <ClubOwnerView id={id} gameState={gameState} drizzle={drizzle} drizzleState={drizzleState}/>;
        } else {
            renderContent = <ClubBuyerView id={id} gameState={gameState} drizzle={drizzle} drizzleState={drizzleState} />;
        }

        const club = gameState.clubs[id];

        const players = club.playerIds.map(id => gameState.players[id]);

        return (
            <div className=" clubDetail-background container-fluid ">
                <ClubInfo gameState={gameState} id={id} />
                <div className="club-details-container container col-lg-10">
                    <ClubInfoExtra gameState={gameState} id={id}/>
                    <ClubInfoStats gameState={gameState} id={id}/>
                    {renderContent}
                    <ClubPlayers players={players} />
                    <ClubAcademy clubOwner={drizzleState.accounts[0]} gameState={gameState} id={id} />
                </div>
            </div>
        );
    }
}

export default ClubDetail

