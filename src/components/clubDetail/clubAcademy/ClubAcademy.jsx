import React from "react";
import '../clubInfo/clubInfo.css';
import { Game } from 'footium-engine';
import ClubCard from "../../allClubs/clubCards/ClubCard";
import { Link } from "react-router-dom";
import ClubPlayerCard from "../clubPlayers/ClubPlayerCard";

class ClubAcademy extends React.Component {

    render() {
        const { gameState, id, clubOwner } = this.props;
        const academyPlayers = Game.getAcademyPlayers(id, 10, new Date())(gameState);
        console.log(academyPlayers);
        return (

            // <div className="club-info  mt-4 p-4">
            //     <div className="offer-info pb-2 d-flex justify-content-center">
            //         <h4>Academy Players:</h4>
            //     </div>
            //
            //     <div className="player-contaier mt-3 d-flex flex-wrap justify-content-center ">
            //         {academyPlayers.map((player) => <Link key={player.firstName} to={`/clubs/${player.clubId}/academyPlayer/${player.birthDate.getFullYear()}/${player.generationId}`}>
            //             <div className="player">{player.lastName}</div>
            //         </Link>)}
            //     </div>
            //
            // </div>

            <div className="player-container">
                <div className="player-container-title">
                    <h3>Academy Players:</h3>
                </div>
                <div className="player-list">
                    {
                        academyPlayers.map(player =>(
                            <Link
                                key={player.firstName}
                                to={`/clubs/${player.clubId}/academyPlayer/${player.birthDate.getFullYear()}/${player.generationId}`}>
                                <ClubPlayerCard lastName={player.lastName} facePictureId={player.facePictureId} />
                            </Link>
                        ))
                    }

                </div>

            </div>
        )
    }
}

export default ClubAcademy
