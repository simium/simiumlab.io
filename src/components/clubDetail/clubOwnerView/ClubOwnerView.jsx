import React from "react";
import '../clubInfo/clubInfo.css';

class ClubOwnerView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            offer: 0
        }
        this.acceptBidHandler = this.acceptBidHandler.bind(this);
    }

    getOfferView() {
        const { drizzle, gameState, id } = this.props;

        const { fromWei } = drizzle.web3.utils;

        if (!gameState.clubs[id].isForSale) {
            return <h4>You have not offered this club for sale</h4>
        } else {
            return <h4>You are offering this club for: {fromWei(gameState.clubs[id].price)} ETH</h4>
        }
    }

    onChangeHandler = (event) => {
        const { value } = event.target;

        if (value.length === 0) {
            this.setState({ offer: 0 });
        } else {
            this.setState({ offer: value });
        }
    }

    getHighestBid() {
        const { drizzle, gameState, id } = this.props;

        const { fromWei } = drizzle.web3.utils;

        if (gameState.clubs[id].highestBid <= 0) {
            return <h4>No one has placed a bid</h4>
        } else {
            return <h4>The highest bid on the club is: {fromWei(gameState.clubs[id].highestBid)} ETH</h4>
        }
    }

    submitOfferHandler = (event) => {
        event.preventDefault();

        const { drizzle, id } = this.props;
        const { offer } = this.state;

        const { toWei } = drizzle.web3.utils;

        if (offer <= 0) {
            alert("Invalid number")
        } else {
            drizzle.contracts.FirstContract.methods.offerClubForSale(id, toWei(offer)).send();
        }
    }

    acceptBidHandler() {
        const { drizzle, id, gameState } = this.props;
        if (gameState.clubs[id].highestBid <= 0) {
            alert("No one has placed a bid.")
        } else {
            drizzle.contracts.FirstContract.methods.acceptBid(id).send();
        }
    }

    render() {

        return (

            <div className="transaction-container-owner  mt-4 p-4">
                <div className="transaction-description pb-2">
                    {this.getOfferView()}
                    {this.getHighestBid()}
                </div>
                <div className="d-flex row justify-content-around ">

                    <form onSubmit={this.submitOfferHandler} className="form-inline my-2 my-lg-0">
                        <input type="text" className="form-control mr-sm-2 ml-2" placeholder="Put an offer"
                               aria-label="offer" onChange={this.onChangeHandler}/>
                            <button type="submit" className="btn btn-primary">Offer</button>
                    </form>
                    <button className="btn btn-primary" onClick={this.acceptBidHandler}>Accept bid</button>


                </div>
            </div>
        )
    }
}

export default ClubOwnerView
