import React from "react";
import '../clubInfo/clubInfo.css'
class ClubNotOwnedView extends React.Component {
    constructor(props) {
        super(props);
        this.takeOwnership = this.takeOwnership.bind(this);
        console.log(props)
    }

    takeOwnership(){
        const { id, drizzle } = this.props;

        drizzle.contracts.FirstContract.methods.takeOwnership(id).send();
    }

    render() {

        return (

            <div className="transaction-container d-flex justify-content-between align-items-center">
                <div className="transaction-description">
                    <h4>This club has not been owned by any one.</h4>
                </div>
                <div className="btn btn-primary" onClick={this.takeOwnership}>
                    Claim this club
                </div>
            </div>
        )
    }
}

export default ClubNotOwnedView