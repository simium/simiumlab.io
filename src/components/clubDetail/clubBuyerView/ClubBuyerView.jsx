import React from "react";
import '../clubInfo/clubInfo.css'

class ClubBuyerView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            bid: 0
        };

        this.buyClubHandler = this.buyClubHandler.bind(this);
    }


    putBidHandler = (event) => {
        event.preventDefault();

        const { id, drizzle, gameState } = this.props;
        const { bid } = this.state;

        const { toWei, fromWei } = drizzle.web3.utils;

        const { highestBid } = gameState.clubs[id];

        if (bid <= 0) {
            alert("You cannot submit a bid of 0.");
        } else if (bid <= fromWei(highestBid.toString())) {
            alert("You must submit a higher bid than the current highest bid.");
        }else {
            drizzle.contracts.FirstContract.methods.placeBid(id).send({
                value: toWei(bid)
            });
        }
    }

    onChangeHandler = (event) => {
        const { value } = event.target;

        if (value.length === 0) {
            this.setState({ bid: 0 });
        } else {
            this.setState({ bid: value });
        }
    }

    getBuyView(clubId) {
        const { drizzle, gameState } = this.props;

        const { fromWei } = drizzle.web3.utils;

        if (!gameState.clubs[clubId].isForSale) {
            return <h4>This club is currently not for sale</h4>
        } else {
            return <h4>This club is currently offered for {fromWei(gameState.clubs[clubId].price)} ETH</h4>
        }
    }

    buyClubHandler(){
        const { drizzle, gameState, id} = this.props;

        if (!gameState.clubs[id].isForSale) {
            alert("The owner has not put a price, this club is not for sale.")
        } else {
            // alert("this price is" + gameState.clubs[id].price)
            drizzle.contracts.FirstContract.methods.buyClub(id).send({
                value: parseInt(gameState.clubs[id].price)
            });
        }
    }

    render() {
        const { drizzle, drizzleState, gameState, id } = this.props;

        const { fromWei } = drizzle.web3.utils;
        
        const highestBid = fromWei(gameState.clubs[id].highestBid.toString());

        return (
            <div className="transaction-container-owner  mt-4 p-4">
                <div className="transaction-description pb-2">
                    {this.getBuyView(id)}
                    <h4>The highest bid is: {highestBid} ETH</h4>
                </div>
                <div className="d-flex row justify-content-around ">
                    <form className="form-inline my-2 my-lg-0" onSubmit={this.putBidHandler}>
                        <input type="text" className="form-control mr-sm-2 ml-2" placeholder="Put a bid"
                               aria-label="offer" onChange={this.onChangeHandler}/>
                            <button type="submit" className="btn btn-primary">Bid</button>
                    </form>
                    <button className="btn btn-primary" onClick={this.buyClubHandler}>Buy Club</button>
                </div>
            </div>
        )
    }
}

export default ClubBuyerView
