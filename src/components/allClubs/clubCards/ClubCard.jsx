import React from "react";
import './clubCard.css'
import {Link} from 'react-router-dom';

class ClubCard extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { club } = this.props;

        return (
            <div className=" col-lg-3 col-md-4">

                <Link to={`/clubs/${club.id}`}>
                    <div className="card club-card mb-4 box-shadow">
                        <div className="club-img-in-card d-flex justify-content-center ">
                            <img src={`https://jordanlord.co.uk/footium/club-${club.clubBadgeId}.png`} width="90%" height="" alt="the img"/>
                        </div>
                        <div className="card-body">
                            <div className="club-info-name">
                                <h5 className="club-name">{club.name}</h5>
                            </div>
                            <div className="club-info-ownership d-flex column justify-content-between">
                                <h6 className="club-id"># {club.id}</h6>
                                <h6 className="club-if-owned">{club.owner != null ? club.owner : "Not Owned"}</h6>
                            </div>
                        </div>
                    </div>
                </Link>
            </div>
        )
    }
}

export default ClubCard
