import React from 'react';
import './allClubs.css'
import * as R from 'ramda';
import ClubCard from "./clubCards/ClubCard";
import DisplayClubs from "../displayClubs/DisplayClubs";

class AllClubs extends React.Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        const { drizzle } = this.props;

        // const ownerOf = drizzle.contracts.FirstContract.methods.ownerOf(321).call().then((result) => {
        //     this.setState({ ownerOf: result });
        // }).catch(error => {
        //     console.log("ERROR");
        // });

    }

    render() {
        const { drizzle, gameState } = this.props;
        const keys = Object.keys(gameState.clubs);
        console.log(keys);
        return (
            <div className="allClub-background container-fluid ">
                <div className="col-lg-10 col-md-10 mx-auto">
                    <div className="all-clubs-header-container col-lg-6 col-md-8 mx-auto bg-black mt-3 mb-5">
                        <h1 className="all-clubs-header">All Clubs</h1>
                    </div>

                    <div className="container">
                        <div className="row">
                            {/*<ClubCard drizzle={drizzle} club={gameState.clubs[1]} key={1}/>*/}
                            {keys.map((clubId) => <ClubCard drizzle={drizzle} club={gameState.clubs[clubId]} key={clubId}/>)}
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default AllClubs